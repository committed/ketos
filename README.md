# Ketos

Ketos is a application for text analytics, using the Baleen entity extractor. It is a set of plugins which build on top of the Invest framework.

## Getting starting

1. Install Java, Maven and Yarn.
2. Run `./setup-dev-env.sh`
3. Run `./build.sh`
4. Ingest some data with Baleen (see documentation and the example pipelines in `config/baleen`)
5. Run the application `cd build; ./run.sh`
6. View in the browser `open http://localhost:8080`

## Documentation

You can find user and developer documentation under `/documentation`

You can read the documentation by running:

```
cd documentation/website
yarn install
yarn start
```
