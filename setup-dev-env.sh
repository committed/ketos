#!/bin/bash

# Install some global helpers
yarn global add lerna typedoc 

# Setup the Web side with a yarn install to get all the dependencies
yarn bootstrap

# Do a compile of the Java code which will fetch dependencies
mvn compile -DskipTests

# Install documentation too
cd documentation/website
yarn install
cd -