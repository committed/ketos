#!/bin/bash

##########################################


rm -rf build/ui
mkdir -p build/ui

echo "Building UI in Javascript"
cd ketos-js

yarn build
cd -

cp -r ketos-js/packages/ketos-feedback-form/build build/ui/ketos-feedback-form
cp -r ketos-js/packages/ketos-feedback-reader/build build/ui/ketos-feedback-reader
cp -r ketos-js/packages/ketos-ui-corpuslist/build build/ui/ketos-ui-corpuslist
cp -r ketos-js/packages/ketos-ui-corpussummary/build build/ui/ketos-ui-corpussummary
cp -r ketos-js/packages/ketos-ui-documentreader/build build/ui/ketos-ui-documentreader
cp -r ketos-js/packages/ketos-ui-documentsearch/build build/ui/ketos-ui-documentsearch
cp -r ketos-js/packages/ketos-ui-subjectoverview/build build/ui/ketos-ui-subjectoverview
cp -r ketos-js/packages/ketos-ui-mentionsearch/build build/ui/ketos-ui-mentionsearch
cp -r ketos-js/packages/ketos-ui-entitysearch/build build/ui/ketos-ui-entitysearch
cp -r ketos-js/packages/ketos-ui-entitydetails/build build/ui/ketos-ui-entitydetails
cp -r ketos-js/packages/ketos-ui-mentiondetails/build build/ui/ketos-ui-mentiondetails
cp -r ketos-js/packages/ketos-ui-map/build build/ui/ketos-ui-map
cp -r ketos-js/packages/ketos-ui-metadataexplorer/build build/ui/ketos-ui-metadataexplorer
cp -r ketos-js/packages/ketos-ui-relationsearch/build build/ui/ketos-ui-relationsearch
cp -r ketos-js/packages/ketos-ui-relationdetails/build build/ui/ketos-ui-relationdetails
cp -r ketos-js/packages/ketos-ui-network/build build/ui/ketos-ui-network
cp -r ketos-js/packages/ketos-ui-documentdetails/build build/ui/ketos-ui-documentdetails
cp -r ketos-js/packages/ketos-ui-editor/build build/ui/ketos-ui-editor
cp -r ketos-js/packages/ketos-ui-usermanagement/build build/ui/ketos-ui-usermanagement
cp -r ketos-js/packages/ketos-ui-cluster/build build/ui/ketos-ui-cluster

# Delete all the source maps as they are huge
find ./build/ui/ -name "*.map" -type f -delete
