rem Install some global helpers
yarn global add lerna typedoc 

rem Setup the Web side with a yarn install to get all the dependencies
yarn bootstrap

rem Do a compile of the Java code which will fetch dependencies
mvn compile -DskipTests

rem Install documentation too
cd documentation\website
yarn install
cd ..\..