rem Delete old documentation
rmdir /s build\documentation

rem build new documenentation
cd documentation\website
yarn build
cd ..\..

rem Copy the documentation into place
mkdir build
copy /s /e documentation\website\build build\documentation

