#!/bin/bash

##########################################


rmdir /s build\ui
mkdir build\ui

echo "Building UI in Javascript"
cd ketos-js

yarn build
cd -

copy /s /e ketos-js\packages\ketos-feedback-form\build build\ui\ketos-feedback-form
copy /s /e ketos-js\packages\ketos-feedback-reader\build build\ui\ketos-feedback-reader
copy /s /e ketos-js\packages\ketos-ui-corpuslist\build build\ui\ketos-ui-corpuslist
copy /s /e ketos-js\packages\ketos-ui-corpussummary\build build\ui\ketos-ui-corpussummary
copy /s /e ketos-js\packages\ketos-ui-documentreader\build build\ui\ketos-ui-documentreader
copy /s /e ketos-js\packages\ketos-ui-documentsearch\build build\ui\ketos-ui-documentsearch
copy /s /e ketos-js\packages\ketos-ui-subjectoverview\build build\ui\ketos-ui-subjectoverview
copy /s /e ketos-js\packages\ketos-ui-mentionsearch\build build\ui\ketos-ui-mentionsearch
copy /s /e ketos-js\packages\ketos-ui-entitysearch\build build\ui\ketos-ui-entitysearch
copy /s /e ketos-js\packages\ketos-ui-entitydetails\build build\ui\ketos-ui-entitydetails
copy /s /e ketos-js\packages\ketos-ui-mentiondetails\build build\ui\ketos-ui-mentiondetails
copy /s /e ketos-js\packages\ketos-ui-map\build build\ui\ketos-ui-map
copy /s /e ketos-js\packages\ketos-ui-metadataexplorer\build build\ui\ketos-ui-metadataexplorer
copy /s /e ketos-js\packages\ketos-ui-relationsearch\build build\ui\ketos-ui-relationsearch
copy /s /e ketos-js\packages\ketos-ui-relationdetails\build build\ui\ketos-ui-relationdetails
copy /s /e ketos-js\packages\ketos-ui-network\build build\ui\ketos-ui-network
copy /s /e ketos-js\packages\ketos-ui-documentdetails\build build\ui\ketos-ui-documentdetails
copy /s /e ketos-js\packages\ketos-ui-editor\build build\ui\ketos-ui-editor
copy /s /e ketos-js\packages\ketos-ui-usermanagement\build build\ui\ketos-ui-usermanagement
copy /s /e ketos-js\packages\ketos-ui-cluster\build build\ui\ketos-ui-cluster

REM TODO Delete all the source maps as they are huge
REM find .\build\ui' -name "*.map" -type f -delete
